@extends('layouts.app')

@section('content')
    <section id="home" class="hero is-fullheight is-default is-bold">
        <div class="hero-body">
            <div class="container">
                <div id="main_hero" class="columns is-vcentered">
                    <div class="column is-5 is-offset-1 landing-caption">
                        <h1 class="title is-1 is-bold is-spaced">
                            Beheren, Inzetten.
                        </h1>
                        <h2 class="subtitle is-5 is-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Delectus dicta dolore exercitationem libero molestiae nam quasi quibusdam quo repellat
                            repudiandae.</h2>
                        <div class="button-wrap">
                            <a href="#pricing" class="button cta is-rounded primary-btn raised">
                                Begin Nu
                            </a>
                            <a href="#features" class="button cta is-rounded">
                                Ontdek
                            </a>
                        </div>
                    </div>
                    <div class="column is-5">
                        <figure class="image is-4by3">
                            <img src="{{ asset('/img/illustrations/worker.svg') }}" alt="Description">
                        </figure>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="features" class="section section-feature-grey is-medium">
        <div class="container">
            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2">Grote kracht komt</h2>
                <h3 class="subtitle is-5 is-muted">Met grote Verantwoordelijkheid</h3>
                <div class="divider is-centered"></div>
            </div>

            <div id="spec_hero" class="content-wrapper">
                <div class="columns">
                    <div class="column is-one-third">
                        <div class="feature-card is-bordered has-text-centered is-feature-reveal">
                            <div class="card-title">
                                <h4>App builder</h4>
                            </div>
                            <div class="card-icon">
                                <img src="{{ asset('/img/icons/web.svg') }}">
                            </div>
                            <div class="card-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="card-action">
                                <a href="#contact" class="button btn-align-md primary-btn raised">Gratis proef periode!</a>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="feature-card is-bordered has-text-centered is-feature-reveal">
                            <div class="card-title">
                                <h4>Cloud integration</h4>
                            </div>
                            <div class="card-icon">
                                <img src="{{ asset('/img/icons/rocket.svg') }}">
                            </div>
                            <div class="card-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="card-action">
                                <a href="#pricing" class="button btn-align-md primary-btn raised">Start nu!</a>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="feature-card is-bordered has-text-centered is-feature-reveal">
                            <div class="card-title">
                                <h4>Addons & Plugins</h4>
                            </div>
                            <div class="card-icon">
                                <img src="{{ asset('/img/icons/light-bulb.svg') }}">
                            </div>
                            <div class="card-text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                            <div class="card-action">
                                <a href="#pricing" class="button btn-align-md primary-btn raised">Start nu!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section is-medium">
        <div class="container">

            <div id="example_hero" class="columns is-vcentered app-side">
                <div class="column is-5 is-offset-1">
                    <h3 class="title is-3 is-spaced is-title-reveal">Een intuïtief app</h3>
                    <p class="subtitle is-5 is-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum
                        audissem Antiochum, Brute, ut solebam, cum M. Quae diligentissime contra Aristonem dicuntur a
                        Chryippo.</p>
                </div>
                <div class="column is-10">
                    <div class="has-text-centered">
                        <img class="pushed-image" src="{{ asset('/img/illustrations/mockups/app-mockup.png') }}">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="reviews" class="section is-medium  has-background-image"
             data-background="https://source.unsplash.com/g30P1zcOzXo/1600x900" data-color="#4FC1EA"
             data-color-opacity=".6">
        <div class="overlay"></div>
        <div class="container">

            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2 light-text is-spaced">Onze klanten houden van ons</h2>
                <h3 class="subtitle is-5 light-text">Kijk wat ze over ons zeggen</h3>
            </div>

            <div id="reviews_hero" class="content-wrapper">
                <div class="columns is-vcentered">
                    <div class="column is-4">
                        <figure class="testimonial">
                            <blockquote>
                                Lorem ipsum dolor sit amet, elit deleniti dissentias quo eu, hinc minim appetere te usu,
                                ea case duis scribentur has. Duo te consequat elaboraret, has quando suavitate at.
                            </blockquote>
                            <div class="author">
                                <img src="{{ asset('/img/illustrations/faces/1.png') }}" alt=""/>
                                <h5>Irma Walters</h5><span>Accountant</span>
                            </div>
                        </figure>
                    </div>
                    <div class="column is-4">
                        <figure class="testimonial">
                            <blockquote>
                                Lorem ipsum dolor sit amet, elit deleniti dissentias quo eu, hinc minim appetere te usu,
                                ea case duis scribentur has. Duo te consequat elaboraret, has quando suavitate at.
                            </blockquote>
                            <div class="author">
                                <img src="{{ asset('/img/illustrations/faces/2.png') }}" alt=""/>
                                <h5>John Bradley</h5><span>Financieel Analist</span>
                            </div>
                        </figure>
                    </div>
                    <div class="column is-4">
                        <figure class="testimonial">
                            <blockquote>
                                Lorem ipsum dolor sit amet, elit deleniti dissentias quo eu, hinc minim appetere te usu,
                                ea case duis scribentur has. Duo te consequat elaboraret, has quando suavitate at.
                            </blockquote>
                            <div class="author">
                                <img src="{{ asset('/img/illustrations/faces/3.png') }}" alt=""/>
                                <h5>Gary Blackman</h5><span>HR Manager</span>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="pricing" class="section is-medium">
        <div class="container">
            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2">Start nu!</h2>
                <h3 class="subtitle is-5 is-muted">Kies een van onze paketten</h3>
                <div class="divider is-centered"></div>
            </div>

            <div id="packages_hero" class="pricing-wrap">
                @foreach($products as $product)
                    <div class="feature-card is-pricing has-text-centered">
                        <h3 class="plan-name">{{ $product->name }}</h3>
                        <img src="{{ asset($product->image) }}" alt=""/>
                        <div class="price">
                            {{ $product->price / 100 }}
                        </div>
                        <p>{{ $product->description }}</p>
                        <div class="field has-addons columns">
                            <div class="control column is-6">
                                <button onclick="addCart({{ $product->id }})" class="button is-bold">Toevoegen</button>
                            </div>
                            <div class="control column is-6">
                                <input class="input" type="number" id="productAmount_{{ $product->id }}" value="1" min="1">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="contact" class="section section-light-grey is-medium">
        <div class="container">
            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2 is-spaced">Neem contact met ons op!</h2>
                <h3 class="subtitle is-5 is-muted">We horen graag van je</h3>
                <div class="divider is-centered"></div>
            </div>

            <div id="contact_hero" class="content-wrapper">
                <div class="columns">
                    <div class="column is-6 is-offset-3">
                        <form id="contact">
                            @if($errors->any())
                                <div class="tab-content">
                                    <article class="message is-danger">
                                        <div class="message-body">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </article>
                                </div>
                            @endif
                            <div class="columns is-multiline">
                                <div class="column is-6">
                                    <input name="first_name" class="input is-medium" type="text" placeholder="Voornaam *" required>
                                </div>
                                <div class="column is-6">
                                    <input name="last_name" class="input is-medium" type="text" placeholder="Achternaam *" required>
                                </div>
                                <div class="column is-6">
                                    <input name="email" class="input is-medium" type="email" placeholder="Email *" required>
                                </div>
                                <div class="column is-6">
                                    <input name="company" class="input is-medium" type="text" placeholder="Bedrijfsnaam">
                                </div>
                                <div class="column is-12">
                                    <textarea name="message" class="textarea" rows="6" placeholder="" required></textarea>
                                </div>
                                <div class="column is-12">
                                    <div class="form-footer has-text-right mt-10">
                                        <button id="sendMessage" class="button cta is-large primary-btn form-button raised rounded is-clear">
                                            Verstuur bericht
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('modals.products.small-package')

    <script type="application/javascript">

        ScrollReveal().reveal('#main_hero', { delay: 500 });
        ScrollReveal().reveal('#spec_hero', { delay: 500 });
        ScrollReveal().reveal('#example_hero', { delay: 500 });
        ScrollReveal().reveal('#reviews_hero', { delay: 500 });
        ScrollReveal().reveal('#packages_hero', { delay: 500 });
        ScrollReveal().reveal('#contact_hero', { delay: 500 });

        function addCart(productID) {
            $.ajax({
                type: 'POST',
                url: '/api/cart/add',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    product: productID,
                    productAmount: $('#productAmount_' + productID).val()
                },
                success: function (data) {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: data.code,
                    });
                },
                error: function (data) {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: data.code,
                    });
                }
            });
        }

        $(document).ready(function () {
            $('form#contact').submit(function (event) {
                event.preventDefault();

                let oneIsEmpty = false;
                $('form#contact :input').each(function () {
                    let input = $(this);

                    console.log(input)

                    if (!input.val() && input[0].nodeName !== 'BUTTON' && input[0].name !== 'company') {
                        input.addClass('is-danger');
                        oneIsEmpty = true;
                    }
                })

                if (!oneIsEmpty) {
                    $.ajax({
                        type: 'POST',
                        url: '/api/contact',
                        data: $('form#contact').serializeArray(),
                        encode: true,
                        success: function (data) {
                            Swal.fire({
                                title: data.title,
                                text: data.message,
                                icon: data.code,
                            }).then((value) => {
                                if (value) {
                                    $('form#contact :input').each(function () {
                                        $(this).val('');
                                    })
                                }
                            });
                        },
                        error: function (data) {
                            Swal.fire({
                                title: data.responseJSON.title,
                                text: data.responseJSON.message,
                                icon: data.responseJSON.code,
                            });
                        }
                    })
                }
            })
        });
    </script>

@endsection
