<footer class="footer footer-dark">
    <div class="container">
        <div class="columns">
            <div class="column">
                <div class="footer-logo">
                    <h1 class="title is-4 has-text-weight-bold has-text-white">Luther Montage</h1>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Product</h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="#">Ontdek onze features</a></li>
                        <li><a href="#">Waarom ons product?</a></li>
                        <li><a href="#">Vergelijk features</a></li>
                        <li><a href="#">Onze Roadmap</a></li>
                        <li><a href="#">Vraag features aan</a></li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Docs</h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="#">Start hier</a></li>
                        <li><a href="#">Gebruikershandleiding</a></li>
                        <li><a href="#">Admin handleiding</a></li>
                        <li><a href="#">Developers</a></li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Blogroll</h3>
                    </div>
                    <ul class="link-list">
                        <li><a href="#">Laatste news</a></li>
                        <li><a href="#">Tech artikelen</a></li>
                        <li><a href="#">Video Blog</a></li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <div class="footer-column">
                    <div class="footer-header">
                        <h3>Volg ons</h3>
                        <nav class="level is-mobile">
                            <div class="level-left">
                                <a class="level-item" href="https://github.com/#">
                                        <span class="icon">
                                            <ion-icon name="logo-github" size="large"></ion-icon>
                                        </span>
                                </a>
                                <a class="level-item" href="https://facebook.com/#">
                                        <span class="icon">
                                            <ion-icon name="logo-facebook" size="large"></ion-icon>
                                        </span>
                                </a>
                                <a class="level-item" href="https://google.com/#">
                                        <span class="icon">
                                            <ion-icon name="logo-google" size="large"></ion-icon>
                                        </span>
                                </a>
                                <a class="level-item" href="https://linkedin.com/#">
                                        <span class="icon">
                                            <ion-icon name="logo-linkedin" size="large"></ion-icon>
                                        </span>
                                </a>
                            </div>
                        </nav>

                        <a href="https://bulma.io" target="_blank">
                            <img src="{{ asset('img/logo/made-with-bulma.png') }}" alt="Made with Bulma" width="128"
                                 height="24">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Back To Top Button -->
<div x-data="initBackToTop()" x-on:scroll.window="scroll($event)" @click="goTop($event)" id="backtotop"><a
        href="javascript:" :class="{
        'visible': scrolled,
        '': !scrolled
    }"></a></div>

<div x-data="initSidebar()" class="sidebar" :class="{
            'is-active': $store.app.isSiderbarOpen,
            '': !$store.app.isSiderbarOpen
        }">
    <div class="sidebar-header">
        <img src="{{ asset('img/logo/fresh-square.svg') }}">
        <a @click="closeSidebar()" class="sidebar-close" href="javascript:void(0);"><i data-feather="x"></i></a>
    </div>
    <div class="inner">
        <ul class="sidebar-menu">
            <li><span class="nav-section-title"></span></li>

            <li>
                <a href="{{ route('orders.index') }}">
                    <span class="material-icons">local_shipping</span>
                    <span>Bestellingen</span>
                </a>
            </li>
            <li>
                <a href="{{ route('products.index') }}">
                    <span class="material-icons">inventory_2</span>
                    <span>Producten</span>
                </a>
            </li>
            <li>
                <a href="{{ route('forms.index') }}">
                    <span class="material-icons">email</span>
                    <span>Contact aanvragen</span>
                </a>
            </li>

{{--            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-1" class="have-children" :class="{--}}
{{--                        'active': openedMenu === 'sidebar-menu-1',--}}
{{--                        '': openedMenu != 'sidebar-menu-1'--}}
{{--                    }">--}}
{{--                <a href="#">--}}
{{--                    <span class="material-icons">apps</span>--}}
{{--                    <span>Overzicht</span>--}}
{{--                </a>--}}
{{--                <ul x-show.transition="openedMenu === 'sidebar-menu-1'">--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-2" class="have-children" :class="{--}}
{{--                        'active': openedMenu === 'sidebar-menu-2',--}}
{{--                        '': openedMenu != 'sidebar-menu-2'--}}
{{--                    }">--}}
{{--                <a href="#">--}}
{{--                    <span class="material-icons">duo</span>--}}
{{--                    <span>Messages</span>--}}
{{--                </a>--}}
{{--                <ul x-show.transition="openedMenu === 'sidebar-menu-2'">--}}
{{--                    <li><a href="#">Inbox</a></li>--}}
{{--                    <li><a href="#">Compose</a></li>--}}
{{--                    <li><a href="#">Video</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-3" class="have-children" :class="{--}}
{{--                        'active': openedMenu === 'sidebar-menu-3',--}}
{{--                        '': openedMenu != 'sidebar-menu-3'--}}
{{--                    }">--}}
{{--                <a href="#">--}}
{{--                    <span class="material-icons">insert_photo</span>--}}
{{--                    <span>Media</span>--}}
{{--                </a>--}}
{{--                <ul x-show.transition="openedMenu === 'sidebar-menu-3'">--}}
{{--                    <li><a href="#">Library</a></li>--}}
{{--                    <li><a href="#">Upload</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li @click="openSidebarMenu($event)" data-menu="sidebar-menu-4" class="have-children" :class="{--}}
{{--                        'active': openedMenu === 'sidebar-menu-4',--}}
{{--                        '': openedMenu != 'sidebar-menu-4'--}}
{{--                    }">--}}
{{--                <a href="#">--}}
{{--                    <span class="material-icons">policy</span>--}}
{{--                    <span>Settings</span>--}}
{{--                </a>--}}
{{--                <ul x-show.transition="openedMenu === 'sidebar-menu-4'">--}}
{{--                    <li><a href="#">User settings</a></li>--}}
{{--                    <li><a href="#">App settings</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>
