<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Luther Constructie</title>

    {{-- Styles --}}
    <link type="text/css" href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
    <script src="https://kit.fontawesome.com/272646e5db.js" crossorigin="anonymous"></script>

    {{-- Fonts --}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        html {
            scroll-behavior: smooth;
        }
    </style>

</head>

{{-- Scripts --}}
<script src="{{ \Illuminate\Support\Facades\URL::asset('js/app.js') }}"></script>
<script src="{{ asset('js/bundle.js') }}"></script>
<script src="https://unpkg.com/scrollreveal"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.esm.js"></script>

<body>
<div id="app">
    @if(\Illuminate\Support\Facades\Route::is('home'))
        <div id="pageloader" class="pageloader"></div>
        <div id="infraloader" class="infraloader is-active"></div>
    @endif
    {{-- Header --}}
    @include('layouts.header')

    {{-- Content --}}
    @yield('content')

    {{-- Footer --}}
    @include('layouts.footer')

</div>
</body>
</html>
