<div id="confirm-modal" class="modal">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="flex-card auth-card">
            <div class="tabs-wrapper">
                <div id="confirm-tab" class="tab-content is-active">
                    <form id="confirmForm">
                        <div class="field">
                            <label class="label">Je email</label>
                            <div class="control has-icons-left">
                                <input name="email" class="input @error('email') is-danger @enderror" type="email" placeholder="johndoe@gmail.com" value="{{ old('email') }}" required>
                                <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control has-icons-left">
                                <button id="tryConfirm" type="submit" class="button is-fullwidth secondary-btn is-rounded raised">
                                    Krijg toegang
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function closeConfirm() {
        $('#confirm-modal').removeClass('is-active')
        $('html').removeClass('is-clipped')
    }

    function openConfirm() {
        $('#confirm-modal').addClass('is-active')
        $('html').addClass('is-clipped')
    }
</script>
