<div id="#small-pack" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Modal title</p>
            <button id="closeSmallPack" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <!-- Content ... -->
        </section>
        <footer class="modal-card-foot">
            <button id="addToCart" class="button is-success">Save changes</button>
            <button id="close" class="button">Cancel</button>
        </footer>
    </div>
</div>
