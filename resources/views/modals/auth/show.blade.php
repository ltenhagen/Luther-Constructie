<div id="auth-modal" class="modal">
    <div onclick="closeAuth()" class="modal-background modal-dismiss"></div>
    <div class="modal-content">
        <div class="flex-card auth-card">
            <div class="tabs-wrapper">
                <div class="tabs">
                    <ul>
                        <li id="setLogin" class="is-active" data-tab="login-tab"><a>Login</a></li>
                        <li id="setRegister" data-tab="register-tab"><a>Register</a></li>
                    </ul>
                </div>
                @if($errors->any())
                    <div class="tab-content">
                        <article class="message is-danger">
                            <div class="message-body">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </article>
                    </div>
                @endif
                <div id="login-tab" class="tab-content is-active">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="field">
                            <label class="label">Email</label>
                            <div class="control has-icons-left">
                                <input name="email" class="input @error('email') is-danger @enderror" type="email" placeholder="johndoe@gmail.com" value="{{ old('email') }}" required>
                                <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Wachtwoord</label>
                            <div class="control has-icons-left">
                                <input name="password" class="input @error('password') is-danger @enderror" type="password"
                                       placeholder="veilig wachtwoord" required>
                                <span class="icon is-small is-left">
                                      <i class="fas fa-lock"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control has-icons-left">
                                <button type="submit" class="button is-fullwidth secondary-btn is-rounded raised">
                                    Log in
                                </button>
                            </div>
                        </div>
                    </form>

                    {{--                    <a class="button is-fullwidth secondary-btn is-rounded raised">Log in</a>--}}
                </div>
                <div id="register-tab" class="tab-content is-hidden">
                    <form action="{{ route('register') }}" method="POST">
                        @csrf
                        <div class="field">
                            <label class="label">Name</label>
                            <div class="control has-icons-left">
                                <input name="name" class="input @error('name') is-danger @enderror" type="text" placeholder="e.g John Doe" value="{{ old('name') }}" required>
                                <span class="icon is-small is-left">
                                      <i class="fas fa-signature"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Email</label>
                            <div class="control has-icons-left">
                                <input name="email" class="input @error('email') is-danger @enderror" type="email" placeholder="e.g. johndoe@gmail.com"
                                       value="{{ old('email') }}"  required>
                                <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Wachtwoord</label>
                            <div class="control has-icons-left">
                                <input name="password" class="input @error('password') is-danger @enderror" type="password"
                                       placeholder="Veilig wachtwoord" required>
                                <span class="icon is-small is-left">
                                      <i class="fas fa-lock"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Wachtwoord bevestigen</label>
                            <div class="control has-icons-left">
                                <input name="password_confirmation" class="input @error('password_confirmation') is-danger @enderror" type="password"
                                       placeholder="Herhaal wachtwoord" required>
                                <span class="icon is-small is-left">
                                      <i class="fas fa-lock"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="field">
                            <button type="submit" class="button is-fullwidth secondary-btn is-rounded raised">
                                Registreren
                            </button>
                        </div>
                    </form>

                    {{--                    <a class="button is-fullwidth secondary-btn is-rounded raised">Log in</a>--}}
                </div>
            </div>
        </div>
    </div>
    <button onclick="closeAuth()" class="modal-close is-large modal-dismiss" aria-label="close"></button>
</div>

<script>
    $(document).ready(function () {
        $('#setLogin').on('click', function (event) {
            if (!$(this).hasClass('is-active')) {
                $(this).addClass('is-active')
                $('#setRegister').removeClass('is-active')

                $('#login-tab').removeClass('is-hidden').addClass('is-active')
                $('#register-tab').removeClass('is-active').addClass('is-hidden')
            }
        })

        $('#setRegister').on('click', function (event) {
            if (!$(this).hasClass('is-active')) {
                $(this).addClass('is-active')
                $('#setLogin').removeClass('is-active')

                $('#register-tab').removeClass('is-hidden').addClass('is-active')
                $('#login-tab').removeClass('is-active').addClass('is-hidden')
            }
        })
    })

    function openAuth() {
        $('#auth-modal').addClass('is-active');
        $('html').addClass('is-clipped');
    }

    function closeAuth() {
        $('#auth-modal').removeClass('is-active');
        $('html').removeClass('is-clipped');
    }
</script>
