@extends('layouts.app')

@section('content')
    <section class="hero is-fullheight-with-navbar has-background-white-ter">
        <section class="hero-body">
            <div class="container box">

                <div class="title-wrapper has-text-centered">
                    <h2 class="title is-2">Contact aanvragen</h2>
                    <h3 class="subtitle is-5 is-muted">Hier vind je een overzicht van alle contact aanvragen.</h3>
                    <div class="divider is-centered"></div>
                </div>
                <div class="content-wrapper">
                    <div class="columns is-centered">
                        <div class="column has-text-centered">
                            @if($contactForms)
                                <table class="table is-hoverable is-fullwidth">
                                    <thead>
                                    <tr>
                                        <th>Voornaam</th>
                                        <th>Achternaam</th>
                                        <th>Email</th>
                                        <th>Company (optioneel)</th>
                                        <th><abbr title="Klik op de blauwe tekst om het gehele bericht te zien.">Bericht</abbr></th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contactForms as $contactForm)
                                        <tr>
                                            <td class="py-5">{{ $contactForm->first_name }}</td>
                                            <td class="py-5">{{ $contactForm->last_name }}</td>
                                            <td class="py-5">{{ $contactForm->email }}</td>
                                            <td class="py-5">{{ $contactForm->company ?? 'Geen bedrijfsnaam opgegeven' }}</td>
                                            <td class="py-5">
                                                <a onclick="openModal({{ $contactForm->id }})">{{ substr($contactForm->message, 0, 10) . '...' }}</a>
                                            </td>
                                            <td class="py-5">
                                                <div class="select is-info">
                                                    <select autocomplete="off" class="has-text-centered" id="contactStatus{{ $contactForm->id }}" name="status" onchange="onChange({{ $contactForm->id }})">
                                                        <option @if($contactForm->isUnread()) selected @endif value="unread">Niet gelezen</option>
                                                        <option @if($contactForm->isRead()) selected @endif value="read">Gelezen</option>
                                                        <option @if($contactForm->isRepliedTo()) selected @endif value="replied">Op geantwoord</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('admin.modals.forms.view')

                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h1 class="title is-6">Welkom bij de pagina voor het overzicht van de huidige
                                    contact aanvragen, op het moment zijn er geen contact aanvragen beschikbaar.</h1>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <script type="application/javascript">

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        function openModal(contactID) {
            $('#modal_forms' + contactID).addClass('is-active');
            $('html').addClass('is-clipped');
        }

        function closeModal(contactID) {
            $('#modal_forms' + contactID).removeClass('is-active');
            $('html').removeClass('is-clipped');
        }

        function onChange(contactID) {
            let status = $('select#contactStatus' + contactID);

            status.attr('disabled', true)

            console.log(status.val())

            $.ajax({
                type: 'PATCH',
                url: '/api/contact/forms/' + contactID,
                data: { 'status': status.val() },
                success: function (data) {
                    Toast.fire({
                        icon: data.code,
                        text: data.message
                    });

                    status.attr('disabled', false)
                },
                error: function (data) {
                    Toast.fire({
                        icon: data.responseJSON.code,
                        text: data.responseJSON.message
                    });
                }
            })
        }
    </script>
@endsection
