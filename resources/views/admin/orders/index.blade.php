@extends('layouts.app')

@section('content')
    <section class="hero is-fullheight-with-navbar has-background-white-ter">
        <section class="hero-body">
            <div class="container box">

                <div class="title-wrapper has-text-centered">
                    <h2 class="title is-2">Bestellingen</h2>
                    <h3 class="subtitle is-5 is-muted">Hier vind je een overzicht van alle bestellingen.</h3>
                    <div class="divider is-centered"></div>
                </div>
                <div class="content-wrapper">
                    <div class="columns is-centered">
                        <div class="column has-text-centered">
                            @if($orders)
                                <table class="table is-hoverable is-fullwidth">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Status</th>
                                        <th>Betaling Via</th>
                                        <th>Bekijk klant gegevens</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td class="py-5">{{ $order->number }}</td>
                                            <td class="py-5">
                                                <div class="select is-info">
                                                    <select autocomplete="off" class="has-text-centered" id="orderStatus{{ $order->id }}" name="status" onchange="onChange({{ $order->id }})">
                                                        <option @if($order->isProcessing()) selected @endif value="Wordt verwerkt">Wordt verwerkt</option>
                                                        <option @if($order->isInTransitToShippingCompany()) selected @endif value="Onderweg naar bezorgingsbedrijf">Onderweg naar bezorgingsbedrijf</option>
                                                        <option @if($order->inTransitToCustomer()) selected @endif value="Onderweg naar klant">Onderweg naar klant</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td class="py-5">{{ $order->via_bank }}</td>
                                            <td class="py-5">
                                                <button onclick="openModal({{ $order->id }})"
                                                        class="button is-info is-small is-rounded"><i
                                                        class="far fa-edit"></i></button>
                                            </td>
                                        </tr>

                                        @include('admin.modals.orders.contact')

                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h1 class="title is-6">Welkom bij de pagina voor het overzicht van de huidige
                                    bestellingen, op het moment zijn er geen bestellingen.</h1>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <script type="application/javascript">

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        function openModal(orderID) {
            $('#modal_' + orderID).addClass('is-active');
            $('html').addClass('is-clipped');
        }

        function closeModal(orderID) {
            $('#modal_' + orderID).removeClass('is-active');
            $('html').removeClass('is-clipped');
        }

        function onChange(orderID) {
            let status = $('select#orderStatus' + orderID);

            status.attr('disabled', true)

            $.ajax({
                type: 'PATCH',
                url: '/api/orders/' + orderID,
                data: { 'status': status.val() },
                success: function (data) {
                    Toast.fire({
                        icon: data.code,
                        text: data.message
                    });

                    status.attr('disabled', false)
                },
                error: function (data) {
                    Toast.fire({
                        icon: data.responseJSON.code,
                        text: data.responseJSON.message
                    });
                }
            })
        }
    </script>
@endsection
