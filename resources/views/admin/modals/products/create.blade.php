<div class="modal" id="modal_newProduct">
    <div onclick="cancelProduct()" class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Product aanpassen</p>
            <button onclick="cancelProduct()" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <form id="newProduct">
                <div class="columns">
                    <div class="column is-6">
                        <div class="field">
                            <label for="name" class="label">Naam</label>
                            <div class="control">
                                <input name="name" class="input" type="text" placeholder="Pakket..." required>
                            </div>
                        </div>
                    </div>

                    <div class="column is-6">
                        <div class="field">
                            <label for="price" class="label">Prijs</label>
                            <div class="control">
                                <input name="price" class="input" type="number" placeholder="9.99" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="columns is-fullwidth">
                    <div class="column is-12">
                        <div class="field">
                            <label for="description" class="label">Beschrijving</label>
                            <div class="control">
                                <textarea maxlength="57" style="width: 100%; height: 50px" name="description" class="input" placeholder="Schrijf hier je beschrijving..." required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </section>
        <footer class="modal-card-foot">
            <button onclick="storeProduct()" class="button is-success">Save changes</button>
            <button onclick="cancelProduct()" class="button">Cancel</button>
        </footer>
    </div>
</div>
