<div class="modal" id="modal_{{ $product->id }}">
    <div onclick="closeModal({{ $product->id }})" class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Product aanpassen</p>
            <button onclick="closeModal({{ $product->id }})" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <form id="update_{{ $product->id }}">
                <div class="columns">
                    <div class="column is-6">
                        <div class="field">
                            <label for="name" class="label">Naam</label>
                            <div class="control">
                                <input name="name" class="input" type="text" value="{{ $product->name }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="column is-6">
                        <div class="field">
                            <label for="price" class="label">Prijs</label>
                            <div class="control">
                                <input name="price" class="input" type="number" value="{{ $product->price / 100 }}" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="columns is-fullwidth">
                    <div class="column is-12">
                        <div class="field">
                            <label for="description" class="label">Beschrijving</label>
                            <div class="control">
                                <textarea style="width: 100%; height: 50px" name="description" class="input" required>{{ $product->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="image" value="{{ $product->image }}">

            </form>
        </section>
        <footer class="modal-card-foot">
            <button onclick="update({{ $product->id }})" class="button is-success">Save changes</button>
            <button onclick="closeModal({{ $product->id }})" class="button">Cancel</button>
        </footer>
    </div>
</div>
