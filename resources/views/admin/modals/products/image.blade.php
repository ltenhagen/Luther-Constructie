<div class="modal" id="image_{{ $product->id }}">
    <div onclick="closeImage({{ $product->id }})" class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Product Image</p>
            <button onclick="closeImage({{ $product->id }})" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <img draggable="false" src="{{ asset($product->image) }}" alt="{{ $product->name }}">
        </section>
        <footer class="modal-card-foot">
            <button onclick="closeImage({{ $product->id }})" class="button is-info">Close</button>
        </footer>
    </div>
</div>
