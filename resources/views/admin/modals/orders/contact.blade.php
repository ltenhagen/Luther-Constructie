<div class="modal" id="modal_{{ $order->id }}">
    <div onclick="closeModal({{ $order->id }})" class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Klant gegevens</p>
            <button onclick="closeModal({{ $order->id }})" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="columns is-centered">
                <div class="column is-6">
                    <div class="field">
                        <label for="email" class="label">E-mailadres *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="email" disabled value="{{ $order->contact->email }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="first_name" class="label">Voornaam *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="first_name" disabled value="{{ $order->contact->first_name }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-signature"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="country" class="label">Land *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="country" disabled value="{{ $order->contact->country }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-signature"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="address" class="label">Adres *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="address" disabled value="{{ $order->contact->address }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-map-pin"></i>
                                        </span>
                        </div>
                    </div>
                </div>
                <div class="column is-6">
                    <div class="field">
                        <label for="phone_number" class="label">Telefoonnummer *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="phone_number" disabled value="{{ $order->contact->phone_number }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-mobile-alt"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="last_name" class="label">Achternaam *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="last_name" disabled value="{{ $order->contact->last_name }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-signature"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="postal_code" class="label">Postcode *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="postal_code" disabled value="{{ $order->contact->postal_code }}">
                            <span class="icon is-small is-left">
                                            <i class="fab fa-usps"></i>
                                        </span>
                        </div>
                    </div>

                    <div class="field">
                        <label for="city" class="label">City *</label>
                        <div class="control has-icons-left">
                            <input type="text" class="input" name="city" disabled value="{{ $order->contact->city }}">
                            <span class="icon is-small is-left">
                                            <i class="fas fa-city"></i>
                                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button onclick="closeModal({{ $order->id }})" class="button is-success">Sluiten</button>
{{--            <button onclick="closeModal({{ $order->Id }})" class="button">Close</button>--}}
        </footer>
    </div>
</div>
