<div class="modal" id="modal_forms{{ $contactForm->id }}">
    <div onclick="closeModal({{ $contactForm->id }})" class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Bericht bekijken</p>
            <button onclick="closeModal({{ $contactForm->id }})" class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <form id="newProduct">
                <div class="columns is-fullwidth">
                    <div class="column is-12">
                        <div class="field">
                            <label for="description" class="label">Bericht</label>
                            <div class="control">
                                <textarea maxlength="57" style="width: 100%; height: 250px" name="description" class="input" disabled>{{ $contactForm->message }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </section>
        <footer class="modal-card-foot">
            <button onclick="closeModal({{ $contactForm->id }})" class="button is-success">Sluiten</button>
        </footer>
    </div>
</div>
