@extends('layouts.app')

@section('content')
    <section class="hero is-fullheight-with-navbar has-background-white-ter">
        <section class="hero-body">
            <div class="container box">

                @include('admin.modals.products.create')
                <button onclick="newProduct()" class="button rounded secondary-btn raised">Product toevoegen</button>

                <div class="title-wrapper has-text-centered">
                    <h2 class="title is-2">Producten</h2>
                    <h3 class="subtitle is-5 is-muted">Hier vind je een overzicht van alle producten.</h3>
                    <div class="divider is-centered"></div>
                </div>
                <div class="content-wrapper">
                    <div class="columns is-centered">
                        <div class="column has-text-centered">
                            @if($products)
                                <table class="table is-hoverable is-fullwidth">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Naam</th>
                                        <th>Beschrijving</th>
                                        <th>Prijs</th>
                                        <th>Afbeelding</th>
                                        <th class="has-text-centered">Aanpassen</th>
                                        <th class="has-text-centered">Verwijderen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td class="py-5">{{ $product->number }}</td>
                                            <td class="py-5">{{ $product->name }}</td>
                                            <td class="py-5">{{ $product->description }}</td>
                                            <td class="py-5">&euro; {{ $product->price / 100 }}</td>
                                            <td class="py-5"><a onclick="openImage({{ $product->id }})">Klik hier voor
                                                    de afbeelding</a></td>
                                            <td class="py-5">
                                                <button onclick="openModal({{ $product->id }})"
                                                        class="button is-info is-small is-rounded"><i
                                                        class="far fa-edit"></i></button>
                                            </td>
                                            <td class="py-5">
                                                <button onclick="remove({{ $product->id }})"
                                                        class="button is-danger is-small is-rounded"><i
                                                        class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>

                                        @include('admin.modals.products.edit')
                                        @include('admin.modals.products.image')

                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h1 class="title is-6">Welkom bij de pagina voor het overzicht van de huidige
                                    producten, op het moment zijn er geen producten aangemaakt.</h1>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <script type="application/javascript">

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        function openModal(productID) {
            $('#modal_' + productID).addClass('is-active');
            $('html').addClass('is-clipped');
        }

        function closeModal(productID) {
            $('#modal_' + productID).removeClass('is-active');
            $('html').removeClass('is-clipped');
        }

        function openImage(productID) {
            $('#image_' + productID).addClass('is-active');
            $('html').addClass('is-clipped');
        }

        function closeImage(productID) {
            $('#image_' + productID).removeClass('is-active');
            $('html').removeClass('is-clipped');
        }

        function newProduct() {
            $('#modal_newProduct').addClass('is-active');
            $('html').addClass('is-clipped');
        }

        function cancelProduct() {
            $('form#newProduct :input').each(function () {
                let input = $(this);

                if (input.val()) {
                    return Swal.fire({
                        icon: 'warning',
                        title: 'Pas op!',
                        text: 'Weet je zeker dat je verder wilt gaan? De gegevens worden niet opgeslagen.',
                        showCancelButton: true,
                        confirmButtonText: 'Ja ik weet het zeker'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            closeNewProduct();
                        }
                    })
                }

                closeNewProduct()
            });
        }

        function closeNewProduct() {
            $('#modal_newProduct').removeClass('is-active');
            $('html').removeClass('is-clipped');
        }

        function storeProduct() {
            let data = ($('form#newProduct').serializeArray());

            let oneIsEmpty = false;
            /* Check if information has no empty fields */
            $('form#newProduct :input').each(function () {
                let input = $(this);

                if (!input.val()) {
                    input.addClass('is-danger');
                    oneIsEmpty = true;
                }
            });

            if (!oneIsEmpty) {
                $.ajax({
                    type: 'POST',
                    url: '{{ route('api.products.store') }}',
                    data: {'data': data},
                    success: function (data) {
                        Toast.fire({
                            icon: data.code,
                            title: data.message
                        })

                        closeNewProduct()

                        window.setTimeout(function () {
                            location.reload()
                        }, 3000)
                    },
                    error: function (data) {
                        Toast.fire({
                            icon: data.responseJSON.code,
                            title: data.responseJSON.message
                        });
                    }
                })
            }
        }

        function update(productID) {
            let data = ($('form#update_' + productID).serializeArray());

            $.ajax({
                type: 'PATCH',
                url: '/api/products/' + productID,
                data: {'data': data},
                success: function (data) {
                    Toast.fire({
                        icon: data.code,
                        title: data.message
                    })

                    closeModal(productID)

                    window.setTimeout(function () {
                        location.reload()
                    }, 3000)
                },
                error: function (data) {
                    closeModal(productID);
                    Toast.fire({
                        icon: data.responseJSON.code,
                        title: data.responseJSON.message
                    });
                }
            });
        }

        function remove(productID) {
            Swal.fire({
                title: 'Pas op!',
                text: 'Weet je zeker dat je dit product wilt verwijderen?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ja ik weet het zeker'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'DELETE',
                        url: '/api/products/' + productID,
                        success: function (data) {
                            Toast.fire({
                                icon: data.code,
                                title: data.message
                            })

                            closeModal(productID)

                            window.setTimeout(function () {
                                location.reload()
                            }, 3000)
                        },
                        error: function (data) {
                            closeModal(productID);
                            Toast.fire({
                                icon: data.responseJSON.code,
                                title: data.responseJSON.message
                            });
                        }
                    });
                }
            })
        }
    </script>
@endsection
