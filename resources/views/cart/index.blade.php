@extends('layouts.app')

@section('content')
    <meta name="cart" content="{{ Session::get('cart') }}">
    <style>
        #cart {

        }

        #information {
            display: none;
            opacity: 0;
        }

        #payment {
            display: none;
            opacity: 0;
        }
    </style>

    <form id="cartForm">
        <div id="cart">
            <section class="hero is-fullheight-with-navbar">
                <section class="hero-body">
                    <div class="container box">
                        <div class="title-wrapper has-text-centered">
                            <h2 class="title is-2">Winkelmand</h2>
                            <h3 class="subtitle is-5 is-muted">De plek waar magie plaats vindt.</h3>
                            <div class="divider is-centered"></div>
                        </div>
                        <div class="content-wrapper">
                            <div class="columns is-centered">
                                @if($cart)
                                    @if($cart->products->count() != 0)
                                        <div class="column is-8">
                                            <table class="table is-hoverable is-fullwidth">
                                                <thead>
                                                <tr>
                                                    <th><abbr title="Product nummer">ID</abbr></th>
                                                    <th><abbr title="Product naam">Product</abbr></th>
                                                    <th><abbr title="Product prijs">Prijs</abbr></th>
                                                    <th><abbr title="Product aantal">Aantal</abbr></th>
                                                    <th><abbr title="Totaal voor aantal producten">Totaal</abbr></th>
                                                    <th class="has-text-centered"><abbr
                                                            title="Verwijder product">Verwijderen</abbr>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cart->products as $product)
                                                    <tr>
                                                        <td class="py-5">{{ $product->number }}</td>
                                                        <td class="py-5">{{ $product->name }}</td>
                                                        <td class="py-5">&euro; {{ $product->price / 100 }}</td>
                                                        <td style="width: 12%" class="py-5"><input
                                                                id="productAmount_{{ $product->id }}"
                                                                onchange="changeAmount({{ $product->id }}, {{ $product->price }})"
                                                                type="number"
                                                                value="{{ $product->pivot->amount }}"
                                                                min="1" max="5"
                                                                class="input is-rounded">
                                                        </td>
                                                        <td id="productTotal" class="py-5">
                                                            &euro; {{ round(($product->price / 100) * $product->pivot->amount, 2) }}</td>
                                                        <td class="py-5 has-text-centered">
                                                            <button onclick="remove({{ $product->id }})"
                                                                    class="button is-danger is-small is-rounded"><i
                                                                    class="far fa-trash-alt"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="column is-4">
                                            <div class="container">
                                                <div class="title-wrapper">
                                                    <h2 class="title is-4">Bestelling</h2>
                                                    <h3 class="subtitle is-muted is-6">Hier vind je de gegevens van je
                                                        bestelling.</h3>
                                                </div>
                                                <hr>
                                                <div class="columns">
                                                    <div class="column">
                                                        <table class="table is-fullwidth">
                                                            <tr>
                                                                <td class="has-text-weight-bold">Totaal prijs (excl.
                                                                    btw)
                                                                </td>
                                                                <td class="has-text-left">
                                                                    &euro; {{ round((($total) / 100) * .79, 2) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="has-text-weight-bold">21% BTW</td>
                                                                <td class="has-text-left">
                                                                    &euro; {{ round((($total) / 100) * .21, 2) }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="container">
                                                <div class="columns is-multiline">
                                                    <div class="column">
                                                        <table class="table is-fullwidth">
                                                            <tr>
                                                                <td class="has-text-weight-bold title is-5">Totaal prijs
                                                                </td>
                                                                <td class="has-text-centered">
                                                                    &euro; {{ round((($total) / 100), 2) }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="columns">
                                                    <div class="column">
                                                        <a id="toInformation"
                                                           class="button secondary-btn is-fullwidth raised rounded">Ga
                                                            naar
                                                            de
                                                            volgende stap</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <h1 class="title is-4">Je hebt geen items in je winkelmand.</h1>
                                    @endif
                                @else
                                    <h1 class="title is-4">Je hebt geen items in je winkelmand.</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>

        <div id="information">
            <section class="hero is-fullheight-with-navbar">
                <section class="hero-body">
                    <div class="container box">

                        <a id="returnToCart" class="button rounded">Ga terug</a>

                        <div class="title-wrapper has-text-centered">
                            <h2 class="title is-2">Je gegevens</h2>
                            <h3 class="subtitle is-5 is-muted">De volgende stap voor je bestelling</h3>
                            <div class="divider is-centered"></div>
                        </div>
                        <div class="content-wrapper">
                            <div class="columns is-centered">
                                <div class="column is-3">
                                    <div class="field">
                                        <label for="email" class="label">E-mailadres *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="email" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="first_name" class="label">Voornaam *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="first_name" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-signature"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="country" class="label">Land *</label>
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <input type="hidden" name="country" value="Nederland">
                                                <select name="country" id="country" disabled>
                                                    <option class="input" value="Nederland" selected>Nederland</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="address" class="label">Adres *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="address" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-map-pin"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="column is-3">
                                    <div class="field">
                                        <label for="phone_number" class="label">Telefoonnummer *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="phone_number" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-mobile-alt"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="last_name" class="label">Achternaam *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="last_name" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-signature"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="postal_code" class="label">Postcode *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="postal_code" required>
                                            <span class="icon is-small is-left">
                                            <i class="fab fa-usps"></i>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <label for="city" class="label">City *</label>
                                        <div class="control has-icons-left">
                                            <input type="text" class="input" name="city" required>
                                            <span class="icon is-small is-left">
                                            <i class="fas fa-city"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="columns is-centered">
                                <div class="column is-6">
                                    <a id="toPayment" class="button secondary-btn is-fullwidth raised rounded">Ga naar
                                        betalen</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>


        <div id="payment">
            <section class="hero is-fullheight-with-navbar">
                <section class="hero-body">
                    <div class="container box">

                        <button id="returnToInformation" class="button rounded">Ga terug</button>

                        <div class="title-wrapper has-text-centered">
                            <h2 class="title is-2">Betaling via iDeal</h2>
                            <h3 class="subtitle is-5 is-muted">Het laatste stukje</h3>
                            <div class="divider is-centered"></div>
                        </div>
                        <div class="content-wrapper">
                            <div class="columns is-centered">
                                <p class="title is-5">Selecteer hier uw bank</p>
                            </div>

                            <div class="columns is-centered">
                                <div class="column is-3">
                                    <input onclick="send('Rabobank')" id="rabobank"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="Rabobank">
                                </div>
                                <div class="column is-3">
                                    <input onclick="send('ABN')" id="abn"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="ABN Amro">
                                </div>
                                <div class="column is-3">
                                    <input onclick="send('ING')" id="ing"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="ING">
                                </div>
                            </div>
                            <div class="columns is-centered">
                                <div class="column is-3">
                                    <input onclick="send('ASN')" id="ASN"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="ASN">
                                </div>
                                <div class="column is-3">
                                    <input onclick="send('SNS')" id="SNS"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="SNS">
                                </div>
                                <div class="column is-3">
                                    <input onclick="send('Knab')" id="Knab"
                                           class="button secondary-btn raised rounded is-large is-fullwidth"
                                           type="submit"
                                           value="Knab">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </form>

    <script type="application/javascript">

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        function remove(productID) {
            Swal.fire({
                title: 'Weet je zeker dat je dit item uit je winkelmand wilt halen?',
                showCancelButton: true,
                confirmButtonText: 'Ja',
                icon: 'question'
            }).then((data) => {
                if (data.isConfirmed) {
                    $.ajax({
                        type: 'POST',
                        url: '/api/cart/remove',
                        data: {
                            product: productID,
                            cart: $('meta[name="cart"]').attr('content')
                        },
                        dataType: 'json',
                        encode: true,
                        success: function (data) {
                            Swal.fire(data.title, data.message, data.code)
                            location.reload();
                        },
                        error: function (data) {
                            Swal.fire(data.responseJSON.title, data.responseJSON.message, data.responseJSON.code)
                        }
                    });
                }
            });
        }

        function send(bank) {
            $('#rabobank').attr('disabled', true)
            $('#abn').attr('disabled', true)
            $('#ing').attr('disabled', true)

            $.ajax({
                type: 'POST',
                url: '/api/orders/new',
                data: {
                    via: bank,
                    cart: $('meta[name="cart"]').attr('content'),
                    data: $('form#cartForm').serializeArray()
                },
                encode: true,
                success: function (data) {
                    Swal.fire({
                        title: data.title,
                        text: data.message,
                        icon: data.code,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            location.href = '{{ route('home') }}'
                        }
                    });
                },
                error: function (data) {
                    Swal.fire(data.responseJSON.title, data.responseJSON.message, data.responseJSON.code);
                }
            });
        }

        function changeAmount(productID, productPrice) {
            let inputAmount = $('input#productAmount_' + productID)

            /* Disable input */
            inputAmount.prop('disabled', true);

            productAmount = inputAmount.val();

            /* send AJAX request to update pivot table */
            $.ajax({
                type: 'POST',
                url: '/api/cart/update',
                data: {
                    product: productID,
                    amount: productAmount,
                    cart: $('meta[name="cart"]').attr('content')
                },
                dataType: 'json',
                encode: true,
                success: function (data) {
                    /* Enable input */
                    $('input#productAmount_' + productID).prop('disabled', false);
                    Toast.fire({
                        icon: data.code,
                        text: data.message
                    })

                    window.setTimeout(function () {
                        location.reload()
                    }, 3000)
                },
                error: function (data) {
                    Swal.fire(data.responseJSON.title, data.responseJSON.message, data.responseJSON.code)
                }
            });
        }

        $(document).ready(function () {
            let data = {};

            $('form#cartForm').submit(function (event) {
                event.preventDefault();
            });

            $('#toInformation').on('click', function (event) {
                event.preventDefault();

                $('#cart').fadeOut();

                $('#information').css('display', 'block').css('opacity', 1);
            });

            $('#toPayment').on('click', function (event) {
                event.preventDefault();

                let oneIsEmpty = false;
                /* Check if information has no empty fields */
                $('div#information :input').each(function () {
                    let input = $(this);

                    if (!input.val()) {
                        input.addClass('is-danger');
                        oneIsEmpty = true;
                    }
                });

                if (!oneIsEmpty) {
                    $('#information').fadeOut();
                    $('#payment').css('display', 'block').css('opacity', 1);
                }
            });

            $('#returnToCart').on('click', function (event) {
                event.preventDefault();

                $('#information').fadeOut().css('display', 'none').css('opacity', 0);

                $('#cart').fadeIn().css('display', 'block').css('opacity', 1);
            });

            $('#returnToInformation').on('click', function (event) {
                event.preventDefault();

                $('#payment').fadeOut().css('display', 'none').css('opacity', 0);

                $('#information').fadeIn().css('display', 'block').css('opacity', 1);
            });


            $('#amount').on('change', function (event) {
            });
        });
    </script>
@endsection
