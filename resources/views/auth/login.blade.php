@extends('layouts.app')

@section('content')
    <div id="home" class="hero has-background-white-ter is-fullheight-with-navbar">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-4">
                        <form action="{{ route('login') }}" method="POST" class="box">
                            @csrf
                            <div class="field">
                                <label class="label">Email</label>
                                <div class="control has-icons-left">
                                    <input name="email" class="input" type="email" placeholder="johndoe@gmail.com">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field">
                                <label class="label">Wachtwoord</label>
                                <div class="control has-icons-left">
                                    <input name="password" class="input" type="password"
                                           placeholder="veilig wachtwoord">
                                    <span class="icon is-small is-left">
                                      <i class="fas fa-lock"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field">
                                <div class="control has-icons-left">
                                    <input type="submit" class="button secondary-btn raised is-fullwidth"
                                           value="Log in">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
