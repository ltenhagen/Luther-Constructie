@component('mail::message')
# Je bestelling is geslaagd!

Bedankt voor het plaatsen van je bestelling, dit is je order nummer: {{ $order->number }}.
<hr>
Op onze website kun je ook je bestelling terug zien en de status ervan bij houden.

@component('mail::button', ['url' => $url])
Bekijk hier je bestelling!
@endcomponent

Bedankt,<br>
{{ config('app.name') }}


<small>Als de bovenstaande URL niet werkt klik dan <a href="{{ $url }}">hier</a>!</small>
@endcomponent
