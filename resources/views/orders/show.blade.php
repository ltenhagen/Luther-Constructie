@extends('layouts.app')

@section('content')
    <section class="hero is-fullheight-with-navbar">
        <section class="hero-body">
            <div class="container box">
                <div class="title-wrapper has-text-centered">
                    <h1 class="title is-2">Overzicht van bestelling:</h1>
                    <h2 class="subtitle has-text-grey">{{ $order->number }}</h2>
                    <h3 class="subtitle is-5 is-muted">Bekijk hier de gegevens van je bestelling!</h3>
                    <div class="divider is-centered"></div>
                </div>
                <div class="content-wrapper">
                    <div id="progress" class="is-hidden">
                        <progress class="progress is-info is-large" max="100"></progress>
                    </div>

                    <div id="order" class="columns is-centered">
                        <div class="column is-6">
                            <table class="table is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    <th><abbr title="Product nummer">ID</abbr></th>
                                    <th><abbr title="Product naam">Product</abbr></th>
                                    <th><abbr title="Product prijs">Prijs</abbr></th>
                                    <th><abbr title="Product aantal">Aantal</abbr></th>
                                    <th><abbr title="Totaal voor aantal producten">Totaal</abbr></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order->products as $product)
                                    <tr>
                                        <td class="py-5">{{ $product->number }}</td>
                                        <td class="py-5">{{ $product->name }}</td>
                                        <td class="py-5">&euro; {{ $product->price / 100 }}</td>
                                        <td style="width: 12%" class="py-5">{{ $product->pivot->amount }}
                                        </td>
                                        <td id="productTotal" class="py-5">
                                            &euro; {{ round(($product->price / 100) * $product->pivot->amount, 2) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <table class="table is-fullwidth is-hoverable">
                                <thead>
                                <tr>
                                    <th>Totaal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>&euro; {{ $total }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="column is-6">
                            <table class="table is-fullwidth is-hoverable">
                                <thead>
                                <tr>
                                    <th colspan="2">Contactgegevens</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="has-text-weight-bold">Voornaam:</span>
                                        {{ $order->contact->first_name }}
                                    </td>
                                    <td>
                                        <span class="has-text-weight-bold">Achternaam:</span>
                                        {{ $order->contact->last_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="has-text-weight-bold">Email:</span>
                                        {{ $order->contact->email }}
                                    </td>
                                    <td>
                                        <span class="has-text-weight-bold">Telefoonnummer:</span>
                                        {{ $order->contact->phone_number }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <hr>
                            <table class="table is-fullwidth is-hoverable">
                                <thead>
                                <tr>
                                    <th colspan="2">Adresgegevens</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="has-text-weight-bold">Adres:</span>
                                        {{ $order->contact->address }}
                                    </td>
                                    <td>
                                        <span class="has-text-weight-bold">Postcode:</span>
                                        {{ $order->contact->postal_code }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="has-text-weight-bold">Stad:</span>
                                        {{ $order->contact->city }}
                                    </td>
                                    <td>
                                        <span class="has-text-weight-bold">Land:</span>
                                        {{ $order->contact->country }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table is-hoverable">
                                <thead>
                                <tr>
                                    <th colspan="2">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        {{ $order->status }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    @include('modals.orders.confirm')

    <script type="application/javascript">
        {{--let isConfirmed = false;--}}

        {{--function confirmed() {--}}
        {{--    if (isConfirmed) {--}}
        {{--        $('#progress').addClass('is-hidden')--}}
        {{--        $('#order').removeClass('is-hidden')--}}
        {{--        closeConfirm()--}}
        {{--    }--}}
        {{--}--}}

        {{--$(document).ready(function () {--}}
        {{--    openConfirm()--}}

        {{--    $('form#confirmForm').submit(function (event) {--}}
        {{--        event.preventDefault();--}}

        {{--        let email = $('form#confirmForm :input[type="email"]').val();--}}

        {{--        if (email !== undefined || email !== '') {--}}
        {{--            $.ajax({--}}
        {{--                type: 'POST',--}}
        {{--                url: '{{ route('api.order.confirm', $order->number) }}',--}}
        {{--                data: {--}}
        {{--                    'email': email--}}
        {{--                },--}}
        {{--                encode: true,--}}
        {{--                success: function (data) {--}}
        {{--                    isConfirmed = true;--}}
        {{--                    confirmed()--}}
        {{--                },--}}
        {{--                error: function (data) {--}}
        {{--                    if (data.status !== '403') {--}}
        {{--                        Swal.fire({--}}
        {{--                            icon: data.responseJSON.code,--}}
        {{--                            title: data.responseJSON.title,--}}
        {{--                            text: data.responseJSON.message--}}
        {{--                        }).then((result) => {--}}
        {{--                            location.reload()--}}
        {{--                        })--}}
        {{--                    }--}}

        {{--                    Swal.fire({--}}
        {{--                        icon: data.responseJSON.code,--}}
        {{--                        title: data.responseJSON.title,--}}
        {{--                        text: data.responseJSON.message--}}
        {{--                    }).then((result) => {--}}
        {{--                        window.location = '{{ route('home') }}'--}}
        {{--                    })--}}
        {{--                }--}}
        {{--            })--}}
        {{--        }--}}
        {{--    })--}}
        {{--})--}}
    </script>
@endsection
