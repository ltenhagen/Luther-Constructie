<?php

use App\Http\Controllers\Admin\ArtisanController;
use App\Http\Controllers\Admin\ContactFormController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [PageController::class, 'index'])->name('home');

Route::get('/login', [PageController::class, 'login'])->name('login');
Route::get('/register', [PageController::class, 'register'])->name('register');

Route::get('/cart', [PageController::class, 'cart'])->name('shopping-cart');

Route::get('/orders/{order_guid}', [OrderController::class, 'show'])->name('order.show');
Route::post('/orders/new', [OrderController::class, 'store'])->name('order.new');

Route::group(['prefix' => 'admin', 'middleware' => 'is.admin'], function () {
    Route::resource('orders', App\Http\Controllers\Admin\OrderController::class);
    Route::resource('products', ProductController::class);
    Route::resource('users', UserController::class);
    Route::resource('forms', ContactFormController::class);

    Route::get('artisan/queue/run/worker', [ArtisanController::class, 'worker']);
});
