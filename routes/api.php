<?php

use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/cart/add', [CartController::class, 'add']);
Route::post('/cart/remove', [CartController::class, 'remove']);
Route::post('/cart/update', [CartController::class, 'changeAmount']);

Route::post('/orders/new', [OrderController::class, 'store'])->name('api.orders.new');

Route::post('/contact', [ContactController::class, 'store'])->name('api.contact.new');

Route::post('/products', [ProductController::class, 'store'])->name('api.products.store');
Route::patch('/products/{product}', [ProductController::class, 'update'])->name('api.products.update');
Route::delete('/products/{product}', [ProductController::class, 'destroy'])->name('api.products.delete');

Route::post('/orders/{order_guid}', [OrderController::class, 'confirm'])->name('api.order.confirm');
Route::patch('/orders/{order}', [App\Http\Controllers\Admin\OrderController::class, 'updateStatus'])->name('api.orders.status');

Route::patch('/contact/forms/{id}', [App\Http\Controllers\Admin\ContactFormController::class, 'updateStatus'])->name('api.contact.forms.status');
