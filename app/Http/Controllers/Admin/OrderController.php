<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    function updateStatus(Order $order) {
        if (!\request()->has('status')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        try {
            $order->update([
                'status' => \request()->get('status'),
            ]);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de beheerders.'
            ], 400);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'De status is geupdate!'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('contact')->get();

        return view('admin.orders.index', ['orders' => $orders]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
