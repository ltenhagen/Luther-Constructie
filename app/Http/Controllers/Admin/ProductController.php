<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\InputChecker;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Ramsey\Uuid\Guid\Guid;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('price')->get();

        return view('admin.products.index', ['products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = \request()->get('data');
        if (!$data) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $attributes = [];
        foreach ($data as $item) {
            $attributes[$item['name']] = $item['value'];
        }

        $attributes['price'] = $attributes['price'] * 100;
        $attributes['number'] = random_int(10000, 99999);

        $attributes = InputChecker::check($attributes, ['name', 'price', 'description']);
        if (!$attributes) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        try {
            Product::create($attributes);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er iets mis gegaan, neem contact op met de beheerders.'
            ], 400);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is aangemaakt.'
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Product $product
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Product $product)
    {
        $data = \request()->get('data');
        if (!$data) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $attributes = [];
        foreach ($data as $item) {
            $attributes[$item['name']] = $item['value'];
        }

        $attributes['price'] = $attributes['price'] * 100;

        $attributes = InputChecker::check($attributes, ['name', 'price', 'description', 'image']);
        if (!$attributes) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        try {
            $product->update($attributes);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er iets mis gegaan, neem contact op met de beheerders.'
            ], 400);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is aangepast.'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|void
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er iets mis gegaan, neem contact op met de beheerders.'
            ], 400);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is verwijderd.'
        ], 200);
    }
}
