<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactForm;
use Exception;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    public function updateStatus($id) {
        if (!\request()->has('status')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $contactForm = ContactForm::firstWhere('id', $id)->first();
        if (!$contactForm) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de beheerders.'
            ], 400);
        }

        try {
            $contactForm->update([
                'status' => \request()->get('status'),
            ]);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => $exception->getMessage()
            ], 400);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'De status is geupdate!'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $contactForms = ContactForm::all();

        return view('admin.forms.index', ['contactForms' => $contactForms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
