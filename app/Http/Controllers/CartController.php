<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Guid\Guid;

class CartController extends Controller
{
    public function add()
    {
        if (!\request()->has('product') || !request()->has('productAmount')) {
            return [
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan tijdens het toevoegen van je product in de winkelmand'
            ];
        }

        if (Session::has('cart')) {
            $response = $this->hasCart();
            if (!$response) {
                return [
                    'code' => 'error',
                    'title' => 'Oeps!',
                    'message' => 'Er is iets fout gegaan tijdens het toevoegen van het item aan je winkelmand.'
                ];
            }

            return [
                'code' => 'success',
                'title' => 'Gelukt!',
                'message' => 'Het product is aan je winkelmand toegevoegd!'
            ];
        }

        $response = $this->hasNoCart();
        if (!$response) {
            return [
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan tijdens het toevoegen van het item aan je winkelmand.'
            ];
        }

        return [
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is aan je winkelmand toegevoegd!'
        ];
    }

    private function hasNoCart()
    {
        try {
            $cart = Cart::create(['guid' => Guid::uuid4()]);
            $product = Product::query()->where('id', \request()->get('product'))->firstOrFail();
            $cart->products()->attach($product, array('amount' => request()->get('productAmount')));
        } catch (\Exception $exception) {
            return [
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan tijdens het toevoegen van je product in de winkelmand'
            ];
        }

        Session::put('cart', $cart->guid);

        return [
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is aan je winkelmand toegevoegd!'
        ];
    }

    private function hasCart()
    {
        $cart = Cart::where('guid', Session::get('cart'))->with('products')->firstOrFail();
        $cartProduct = Product::query()->with('carts')->where('id', \request()->get('product'))->firstOrFail();

        $hasProduct = $cart->products()->get()->find($cartProduct);

        if ($hasProduct === null) {
            try {
                $cart->products()->attach($cartProduct, array('amount' => request()->get('productAmount')));
            } catch (\Exception $exception) {
                return false;
            }

            return true;
        }

        foreach ($cart->products as $product) {
            if ($product->id === $cartProduct->id) {
                $amount = $product->pivot->amount;
                $amount += request()->get('productAmount');

                $cart->products()->detach($product);
                $cart->products()->attach($product, array('amount' => $amount));
            }
        }

        return true;
    }

    public function changeAmount() {
        if (!request()->has('product') || !request()->has('amount') || !request()->has('cart')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters!'
            ], 400);
        }

        $product = Product::with('carts')->where('id', request()->get('product'))->first();
        if (!$product) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => $product
            ], 400);
        }

        foreach ($product->carts as $cart) {
            if (request()->get('cart') === $cart->guid) {
                $cart->products()->detach($product);
                $cart->products()->attach($product, ['amount' => request()->get('amount')]);
            }
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product aantal is aangepast!'
        ], 200);
    }

    public function remove() {
        if (!request()->has('product')|| !request()->has('cart')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters!'
            ], 400);
        }

        $product = Product::with('carts')->where('id', request()->get('product'))->first();
        if (!$product) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan.'
            ], 400);
        }

        foreach ($product->carts as $cart) {
            if (request()->get('cart') === $cart->guid) {
                $cart->products()->detach($product);
            }
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Het product is out je winkelmand gehaald!'
        ], 200);
    }
}
