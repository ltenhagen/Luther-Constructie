<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Shows the homepage.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index', ['products' => Product::orderBy('price')->get()]);
    }

    /**
     * Shows the login form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function login() {
        return view('auth.login');
    }

    /**
     * Shows the register form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function register() {
        return view('auth.register');
    }

    public function cart() {
        $cart = null;

        if (Session::has('cart')) {
            $cart = Cart::where('guid', Session::get('cart'))->firstOrFail();

            $total = 0;
            /* TODO: Calculate total price of products in cart, excl btw, 19% btw and total*/
            foreach ($cart->products as $product) {
                $total += $product->price * $product->pivot->amount;
            }

            return view('cart.index', ['cart' => $cart, 'total' => $total]);
        }

        return view('cart.index', ['cart' => $cart]);
    }
}
