<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Mail\OrderCreated;
use App\Models\Cart;
use App\Models\Contact;
use App\Models\Order;
use Exception;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Guid\Guid;
use Session;

class OrderController extends Controller
{
    public function confirm(string $orderGUID) {
        if (!request()->has('email')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $order = Order::where('number', $orderGUID)->first();
        if (!$order) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        if (request()->get('email') !== $order->contact()->first()->email) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Je hebt geen toegang tot deze bestelling.'
            ], 403);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Toegang verleend.'
        ], 200);
    }

    public function show(string $orderGUID) {
        $order = Order::where('number', $orderGUID)->first();
        if (!$order) {
            return redirect('home')->with('message', 'Er is een fout opgetreden tijdens het ophalen van je bestelling, neem contact op met de klantenservice.');
        }

        $total = 0;
        foreach ($order->products as $product) {
            $total += ($product->pivot->amount * round($product->price / 100, 2));
        }

        return view('orders.show', ['order' => $order, 'total' => $total]);
    }

    public function store() {
        if (!\request()->has('via') || !\request()->has('cart')) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $data = \request()->get('data');
        if (!$data) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er missen benodigde parameters.'
            ], 400);
        }

        $attributes = [];
        foreach ($data as $item) {
            $attributes[$item['name']] = $item['value'];
        }

        $contact = Contact::firstWhere('email', $attributes['email']);
        if (!$contact) {
            try {
                $contact = Contact::create($attributes);
            } catch (Exception $exception) {
                return response([
                    'code' => 'error',
                    'title' => 'Oeps!',
                    'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
                ], 500);
            }
        }

        try {
            $order = Order::create([
                'number' => Guid::uuid4(),
                'status' => 'Wordt verwerkt',
                'via_bank' => \request()->get('via')
            ]);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        try {
            $contact->orders()->save($order);
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        $cart = Cart::firstWhere('guid', Session::get('cart'));
        if (!$cart) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        try {
            foreach ($cart->products as $product) {
                $order->products()->attach($product, ['amount' => $product->pivot->amount]);
            }
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        try {
            Session::forget('cart');
            $cart->delete();
        } catch (Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        SendEmail::dispatch($order, $contact->email);

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Je order is geplaatst! Order Nr: ' . $order->number
        ], 200);
    }
}
