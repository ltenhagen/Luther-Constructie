<?php

namespace App\Http\Controllers;

use App\Models\ContactForm;
use App\Models\Product;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store() {
        $attributes = \request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company' => 'nullable',
            'message' => 'required',
        ]);

        $contactForm = ContactForm::firstWhere('email', $attributes['email']);
        if ($contactForm) {
            if ($contactForm->status === 'unread') {
                return response([
                    'code' => 'error',
                    'title' => 'Oeps!',
                    'message' => 'Je hebt nog een openstaand formulier, neem contact op met onze klantenservice hiervoor.'
                ], 400);
            }
        }

        try {
            ContactForm::create($attributes);
        } catch (\Exception $exception) {
            return response([
                'code' => 'error',
                'title' => 'Oeps!',
                'message' => 'Er is iets fout gegaan, neem contact op met de klantenservice.'
            ], 500);
        }

        return response([
            'code' => 'success',
            'title' => 'Gelukt!',
            'message' => 'Je bericht is verstuurd, er word zo snel mogelijk op gereageerd!'
        ], 200);
    }
}
