<?php


namespace App\Helpers;


class InputChecker
{
    public static function check(array $request, array $requiredInputs)
    {
        $missingFields = [];

        foreach ($requiredInputs as $requiredInput) {
            if (!array_key_exists($requiredInput, $request)) {
                $missingFields[] = $requiredInput;
            }
        }

        if (empty($missingFields)) {
            return $request;
        }

        return false;
    }
}
