<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'phone_number',
        'first_name',
        'last_name',
        'country',
        'postal_code',
        'address',
        'city'
    ];

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
