<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'status',
        'via_bank'
    ];

    public function contact() {
        return $this->belongsTo(Contact::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class)->withPivot('amount');
    }

    public function isProcessing() {
        return ($this->status === 'In behandeling');
    }

    public function isInTransitToShippingCompany() {
        return ($this->status === 'Onderweg naar bezorgingsbedrijf');
    }

    public function inTransitToCustomer() {
        return ($this->status === 'Onderweg naar klant');
    }
}
