<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'company',
        'message',
        'status'
    ];

    public function isUnread() {
        return $this->status === 'unread';
    }

    public function isRead() {
        return $this->status === 'read';
    }

    public function isRepliedTo() {
        return $this->status === 'replied';
    }
}
